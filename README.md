# Knowledge Graph Resources

A collection of useful knowledge graph papers, tutorials, etc.

## Links

Links to knowledge graph resources.

### Tools

Knowledge graph and graph database software.

* [TinkerPop3](http://tinkerpop.incubator.apache.org/docs/3.0.1-incubating/) -
  provides graph computing capabilities for both graph databases (OLTP) and
  graph analytic systems (OLAP) under the Apache2 license.
